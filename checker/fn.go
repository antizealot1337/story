package checker

// Fn is a function that implements story.Checker.
type Fn func() bool

// Check by calling the function to get the return value.
func (f Fn) Check() bool { return f() }
