package checker

// Bool implements story.Checker for bool types.
type Bool bool

// Check returns true if the bool is true and false if it is false.
func (b Bool) Check() bool { return bool(b) }
