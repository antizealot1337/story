// Package checker contains a collection of checkers for use in a story.
//
// Checkers are implement story.Checker and are used in a story.Scene.
package checker
