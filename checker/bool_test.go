package checker

import (
	"testing"

	"bitbucket.org/antizealot1337/story"
)

var _ story.Checker = (*Bool)(nil)

func TestBoolCheck(t *testing.T) {
	// Check true
	if !Bool(true).Check() {
		t.Error("Expected true to check out")
	} //if

	// Check false
	if Bool(false).Check() {
		t.Error("Expected false to not check out")
	} //if
} //func
