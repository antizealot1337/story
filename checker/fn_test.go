package checker

import "testing"

func TestFnCheck(t *testing.T) {
	// The value and function
	var (
		v bool
		f = Fn(func() bool { return v })
	)

	// Check the value returned by the check function
	if expected, actual := v, f.Check(); actual != expected {
		t.Errorf("Expected f to consult value v(%v)", expected)
	} //if

	// Set the value to true
	v = true

	// Check the value returned by the check function
	if expected, actual := v, f.Check(); actual != expected {
		t.Errorf("Expected f to consult value v(%v)", expected)
	} //if
} //func
