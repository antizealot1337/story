// Package story is used for building simple branching stories.
//
// There are two way to create a story using this package. The first is to use
// this package as a library and construct the story in Go source code (see
// example below). The other is to use the story mini language and use the
// interpreter in package
// github.com/antizealot1337/story/cmd/storyteller to parse and play the story.
// Please see the README.md for more information about the mini language and
// interpreter as what follows covers the library use case.
//
// When creating a story it's usually easier to use or extend/compose the Stage
// and filling it with Scenes. The Scene is the basic part of a Story as it
// contains the information that is shown to the player. Scenes may contain
// Choices that the player may choose. A choice may be hidden if based on it's
// Avail(). When a choice is chosen its Choose() method is called allowing it to
// act and requesting the next scene to load.
//
// A simple story example:
//
//   s := Stage{
//     Scenes: []Scene{
//       Scene{
//         Name: "First",
//         Disp: "First scene.",
//         Choices: []Choice{
//           Choice{Disp: "Goto next scene.", Next: "Second"},
//         },
//       },
//       Scene{Name: "Second", Disp: "Second scene."},
//     },
//   }
//
// A Story may be played with a Teller as specified under the
// github.com/antizealot1337/story/teller
// package.
package story
