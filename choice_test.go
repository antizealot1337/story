package story

import (
	"testing"

	"bitbucket.org/antizealot1337/story/actor"
	"bitbucket.org/antizealot1337/story/checker"
)

type actFn func()

func (a actFn) Act() { a() } //func

func TestChoiceAvail(t *testing.T) {
	// Create the choice
	c := Choice{}

	// Make sure the choice is available
	if !c.Avail() {
		t.Error("Expected choice to be available")
	} //if

	// Set the checker
	c.Check = checker.Bool(false)

	// Make sure the choice is not available
	if c.Avail() {
		t.Error("Expected the choice to not be available")
	} //if
} //func

func TestChoiceChoose(t *testing.T) {
	// Create the choice
	var (
		v = false
		c = Choice{Next: "next", Action: actor.Fn(func() { v = true })}
	)

	// Check that next is returned
	if expected, actual := c.Next, c.Choose(); actual != expected {
		t.Errorf(`Expected return to be "%s" but was "%s"`, expected, actual)
	} //if

	// Make sure the action was called
	if !v {
		t.Error("Action was not called")
	} //if
} //func
