package parser

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strings"

	"bitbucket.org/antizealot1337/story"
	"bitbucket.org/antizealot1337/story/checker"
)

// Parser will interpret a story.
type Parser struct {
	*bufio.Reader
	world stage
	line  int
} //struct

// NewParser creates a new Parser.
func NewParser(in io.Reader) *Parser {
	return &Parser{
		Reader: bufio.NewReader(in),
	}
} //func

// ReadString reads a string from the Parser until it finds the byte indicated.
func (p *Parser) ReadString(delim byte) (line string, err error) {
	// Read the string
	line, err = p.Reader.ReadString(delim)

	// Check if the error is nil
	if err == nil {
		p.line++ // Increment the line
	} //if
	return
} //func

// Parse parses the Parser.
func (p *Parser) Parse() (story.Story, error) {
	var line string
	var err error

	// Loop through the lines
	for err == nil {
		// Get the line and trim space
		line, err = p.ReadString('\n')

		// Check for an error
		if err != nil {
			continue
		} //if

		switch {
		case strings.HasPrefix(line, "flag"):
			// Parse the flag
			var f flag
			f, err = p.parseFlag(line)

			// Check for an error
			if err != nil {
				return &p.world, err
			} //if

			// Add the flag
			p.world.flags = append(p.world.flags, &f)
		case strings.HasPrefix(line, "scene"):
			// Parse the scene
			var scene story.Scene
			scene, err = p.parseScene(line)

			// Check for an error
			if err != nil {
				return &p.world, err
			} //if

			// Add the scene
			p.world.Scenes = append(p.world.Scenes, scene)
		case line == "":
			// Ignore empty lines
		} //switch
	} //if

	// Check for an error
	if err == io.EOF {
		err = nil
	} //if

	return p.world, err
} //func

func (p *Parser) parseFlag(line string) (f flag, err error) {
	const decllen = len("flag")

	if len(line) < decllen+1 {
		err = p.errAt("parse error: unable to parse flag")
		return
	} //if

	// Remove the "flag" declaration from the line
	line = stripComment(line[decllen+1:])

	// Get an index for the set/clear value
	idx := strings.IndexRune(line, ' ')

	// Check if there was a space
	if idx == -1 {
		line = strings.TrimSpace(line)
		if line == "" {
			err = p.errAt("parse error: missing flag name")
			return
		} //if
		f.name = line
		return
	} //if

	// Set the name
	f.name = strings.TrimSpace(line[:idx])

	switch val := strings.TrimSpace(line[idx:]); val {
	case "set":
		f.val = true
	case "clear":
		// Ignore
	default:
		// Return an error
		err = p.errAtf("parse error: bad flag value", line)
	} //switch

	return
} //func

func (p *Parser) parseScene(line string) (scene story.Scene, err error) {
	const sceneLen = len("scene")

	// Find the ':' and just use that for the name
	idx := strings.IndexRune(line, ':')

	// Set the name
	scene.Name = strings.TrimSpace(line[sceneLen:idx])

	// Start reading lines
	for {
		// Read a line
		line, err = p.ReadString('\n')

		// Check for an error
		if err != nil {
			// Check if the error is an eof
			if err == io.EOF {
				err = nil // Set the error to nil. The reader will figure it out again.
			} //if
			return
		} //if

		line = strings.TrimSpace(line)

		switch {
		case strings.HasPrefix(line, "choice"):
			// Parse the choice
			var choice story.Choice
			choice, err = p.parseChoice(line)

			// Check for an error
			if err != nil {
				return
			} //if

			// Add the choice
			scene.Choices = append(scene.Choices, choice)
		case line == "":
			return
		default:
			// Check if a description was already created
			if scene.Disp == "" {
				scene.Disp = line
			} else {
				scene.Disp += " " + line
			} //if
		} //switch
	} //for
} //func

func (p *Parser) parseChoice(line string) (choice story.Choice, err error) {
	const (
		choiceLen = len("choice")
	)

	// Find the colon index
	idx := strings.IndexRune(line, ':')

	// Make sure the colon index is there
	if idx == -1 {
		err = errors.New("parse error: choice missing colon")
		return
	} //if

	// Get the index of the open and close parens
	oparen := strings.IndexRune(line[:idx], '(')
	cparen := strings.IndexRune(line[:idx], ')')

	// Check for open and close parens
	if oparen != -1 && cparen != -1 {
		// Set the checker
		choice.Check = &flagcheck{
			name:  line[oparen+1 : cparen],
			world: &p.world,
		}
	} else {
		// Set to a checker that always returns true
		choice.Check = checker.Bool(true)
	} //if

	// Get the index of the open and close parens
	oparen = strings.LastIndexByte(line[idx:], '(')
	cparen = strings.LastIndexByte(line[idx:], ')')

	// Check for open and close brackets
	if oparen == -1 && cparen == -1 {
		err = p.errAt("parse error: choice missing next scene")
		return
	} //if

	// Set the description
	choice.Disp = strings.TrimSpace(line[idx+1:][:oparen-1])

	// Set the scene
	choice.Next = strings.TrimSpace(line[idx:][oparen+1 : cparen])

	// Set the action
	choice.Action = &flagact{
		world: &p.world,
	}

	// Get the tail
	line = strings.TrimSpace(line[idx:][cparen+1:])

	// Check for commands
	if len(line) > 0 {
		// Get the index
		idx = strings.IndexRune(line, ' ')

		// Get the action
		act := choice.Action.(*flagact)

		// Check the command
		switch line[:idx] {
		case "set":
			act.action = actionSet
		case "clear":
			act.action = actionClear
		case "toggle":
			act.action = actionToggle
		default:
			err = p.errAtf(`parse error: bad action "%s" `, line[:idx])
			return
		} //switch

		// return

		// Set the name
		act.name = line[idx+1:]
	} //if
	return
} //func

func (p *Parser) errAt(msg string) error {
	return fmt.Errorf("%s at line: %d", msg, p.line)
} //func

func (p *Parser) errAtf(format string, args ...interface{}) error {
	return p.errAt(fmt.Sprintf(format, args...))
} //func

func stripComment(line string) string {
	if idx := strings.Index(line, " #"); idx != -1 {
		return line[:idx]
	} //if
	return line
} //func
