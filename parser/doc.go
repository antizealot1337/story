// Package parser implements a parser for simple text based stories.
//
// The parse parses a simple story "language" and creates a story.Story from
// what it reads. A story according to the mini-language is a collection of
// flags and scenes with scenes having zero or more choices. A flag is a simple
// name that is attached a boolean value that has the state of either set (true)
// or clear (false). Scenes have a name, display, and zero or more choices.
// Choices must have a display and the scene name to which to transition to next
// and may be available based on if a flag is set and may also change the state
// of a flag when selected. All names for flags and scenes are expected to be
// valid printable character without any spaces. Using a space in any name is
// not considered valid. However, any printable UTF-8 runes are valid for use in
// a name.
//
// Flags are declare typically at the top of the file with the structure:
//   flag NAME (set|clear)
// A flag's name is required but set and clear are optional. Flags without set
// or clear provided are automatically assumed to be clear.
//
// Scenes are typically the bulk of the the file's content and are declared
// with the following structure.
//   scene NAME:
//     DISPLAY
//     choice(FLAG_NAME): DISPLAY (NEXT_SCENE) set FLAG_NAME
// Scene definitions span multiple lines. The start of a scene definition is the
// word "scene" followed by the name, a colon, and a new line. The name is
// required. The next zero or more lines are the display. There's no rule
// against an empty display but most scenes will want to display at least
// something. All scenes must be followed by an empty line to indicate the end
// of that scene.
//
// Choices are a little more complex than what is illustrated above.
// They have optional parts and requires parts. A choice is triggered when the
// first word under a scene is "choice". If the choice is dependant on a flag
// being set the flag name should appear in parentheses after the "choice" but
// before the required colon. The display is the text after the colon but before
// the next scene which is also surrounded by parentheses. The next scene is a
// required component. Optinally the choice may change the state of a flag. The
// three modifications are "set", "clear", "toggle" and require the flag name.
// Set will set a flag regardless of its previous state. Clear will clear a flag
// regardless of its previous state. Toggle will flip the flags state (eg set ->
// clear).
//
// Some choices examples:
//   choice: A simple choice. (NEXT_SCENE)
// Dependant on a flag:
//   choice (ACCESS_FLAG_NAME): Depends on a flag being set. (NEXT_SCENE)
// Choices that modify flags:
//    choice: Sets a flag (NEXT_SCENE) set FLAG_NAME
//    choice: Clears a flag (NEXT_SCENE) clear FLAG_NAME
//    choice: Toggles a flag (NEXT_SCENE) toggle FLAG_NAME
package parser
