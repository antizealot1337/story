package parser

import (
	"testing"
)

func TestFlagActAction(t *testing.T) {
	// The name of the flag
	name := "flag"

	// Create a world
	s := stage{
		flags: []*flag{
			&flag{name: name, val: false},
		},
	}

	// Get the flag
	f := s.flags[0]

	// Create a flag action
	fa := flagact{name: name, world: &s, action: actionSet}

	// Act
	fa.Act()

	// Check if the flag was set
	if !f.val {
		t.Error("Expected flag to be set")
	} //if

	// Change the action to clear
	fa.action = actionClear

	// Act
	fa.Act()

	// Check if the flag is clear
	if f.val {
		t.Error("Expected flag to be clear")
	} //if

	// Set the action to toggle
	fa.action = actionToggle

	// Act
	fa.Act()

	// Check if the flag was set
	if !f.val {
		t.Error("Expected flag to be set")
	} //if

	// Act
	fa.Act()

	// Check if the flag is clear
	if f.val {
		t.Error("Expected flag to be clear")
	} //if
} //func
