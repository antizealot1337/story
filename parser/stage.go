package parser

import "bitbucket.org/antizealot1337/story"

// stage is a stage that has flags too.
type stage struct {
	story.Stage
	flags []*flag
} //struct

// getflag attempts to find a Flag with the provided name. If a flag with the
// name found nil is returned.
func (w *stage) getflag(name string) *flag {
	// Loop through the flags
	for _, flag := range w.flags {
		// Check for the name
		if flag.name == name {
			return flag
		} //if
	} //for

	// Return nil since no flag was found
	return nil
} //func
