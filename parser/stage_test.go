package parser

import "testing"

func TestFindFlag(t *testing.T) {
	// The flag name
	flagName := "Test"

	// Create a world
	var w stage

	// Try to find a flag that doesn't exist
	if flag := w.getflag(flagName); flag != nil {
		t.Error("Expected flag to be nil")
	} //if

	// Add a flag
	w.flags = append(w.flags, &flag{name: flagName})

	// Attempt to find the flag by the name
	if flag := w.getflag(flagName); flag == nil {
		t.Error("Expected the find the flag")
	} //if
} //func
