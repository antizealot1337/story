package parser

import "testing"

func TestToggleFlag(t *testing.T) {
	// Create a flag
	var f flag

	// Toggle the flag
	f.toggle()

	// Make sure the flag is set
	if !f.val {
		t.Error("Expected toggle to set flag")
	} //if

	// Toggle the flag
	f.toggle()

	// Make sure the flag is cleared
	if f.val {
		t.Error("Expected toggle to clear the flag")
	} //if
} //func
