package parser

type flagcheck struct {
	name  string
	world *stage
} //struct

func (f *flagcheck) Check() bool {
	// Get the flag
	flag := f.world.getflag(f.name)

	// Check if the flag is empty
	if flag == nil {
		return false
	} //if

	// Return the flag version
	return flag.val
} //func
