package parser

import (
	"testing"
)

func TestFlagCheckCheck(t *testing.T) {
	// The name of the flag
	name := "flag"

	// Create a world
	s := stage{}

	// Create the flag checker
	fc := flagcheck{world: &s, name: name}

	// Make sure the check returns false when it can't find a flag
	// Check the flag checker
	if expected, actual := false, fc.Check(); actual != expected {
		t.Errorf("Expected check to be %v but was %v", expected, actual)
	} //if

	// Set the flags
	s.flags = []*flag{
		&flag{name: name, val: false},
	}

	// Check the flag checker
	if expected, actual := s.flags[0].val, fc.Check(); actual != expected {
		t.Errorf("Expected check to be %v but was %v", expected, actual)
	} //if

	s.flags[0].val = true

	// Check the flag checker
	if expected, actual := s.flags[0].val, fc.Check(); actual != expected {
		t.Errorf("Expected check to be %v but was %v", expected, actual)
	} //if
} //func
