package parser

// actionType represents Action types.
type actionType uint8

const (
	// actionNothing does nothing.
	actionNothing actionType = iota

	// actionSet sets a Flag.
	actionSet

	// actionClear clears a Flag.
	actionClear

	// actionToggle toggles a Flag.
	actionToggle
)

type flagact struct {
	name   string
	action actionType
	world  *stage
} //struct

func (f *flagact) Act() {
	switch f.action {
	case actionClear:
		// Clear the flag
		if flag := f.world.getflag(f.name); flag != nil {
			flag.val = false
		} //if
	case actionSet:
		// Set the flag
		if flag := f.world.getflag(f.name); flag != nil {
			flag.val = true
		} //if
	case actionToggle:
		// Toggle the flag
		if flag := f.world.getflag(f.name); flag != nil {
			flag.toggle()
		} //if
	} //switch
} //func
