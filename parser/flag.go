package parser

// flag is a value for an interpreted story.
type flag struct {
	name string
	val  bool
} //struct

// toggle the Flag.
func (f *flag) toggle() {
	f.val = !f.val
} //func
