package parser

import (
	"strings"
	"testing"
)

func TestParserReadString(t *testing.T) {
	// Create a parser
	p := NewParser(strings.NewReader("one\ntwo\n"))

	// Read a string
	line, err := p.ReadString('\n')

	// Check the error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the line
	if expected, actual := "one\n", line; actual != expected {
		t.Errorf(`Expected line to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the line count
	if expected, actual := 1, p.line; actual != expected {
		t.Errorf("Expected line count to be %d but was %d", expected, actual)
	} //if

	// Read a string
	line, err = p.ReadString('\n')

	// Check the error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the line
	if expected, actual := "two\n", line; actual != expected {
		t.Errorf(`Expected line to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the line count
	if expected, actual := 2, p.line; actual != expected {
		t.Errorf("Expected line count to be %d but was %d", expected, actual)
	} //if

	// Read a string
	_, err = p.ReadString('\n')

	// There should now be an error
	if err == nil {
		t.Error("Expected an error")
	} //if

	// Check the line count
	if expected, actual := 2, p.line; actual != expected {
		t.Errorf("Expected line count to be %d but was %d", expected, actual)
	} //if
} //func

func TestParserParse(t *testing.T) {
	src := `# An example story
flag F1
flag F2 set
flag F3 clear

# This is a simple comment that takes up a whole line

scene SCENE1: # This comment will be ignored too
    This is a scene in the story. It will have choices and since it is the
    first it will be the first one presented to the player.
    choice: This is the players first choice (SCENE2)

scene SCENE2:
    This scene is only shown to the user if the chose the first option. It will
    be the end of the "game" since it contains no choices.

scene SCENE3:
    This scene has choices that are only available if F2 is set and a choice
    that will change the F3 flag.
    choice(F2): This is only available if F2 is set (SCENE4)
    choice: This will flip the F3 flag (SCENE4) set F3

scene SCENE4:
    This is an awesome scene. It is also the end of the game.
`

	// Parse the world
	s, err := NewParser(strings.NewReader(src)).Parse()

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	w, ok := s.(stage)

	if !ok {
		t.Fatal("Expecte story to be an instance of parser.world")
	} //if

	// Check the flags
	if expected, actual := 3, len(w.flags); actual != expected {
		t.Errorf("Expected flag count of %d but was %d", expected, actual)
	} else {
		// TODO: Check the flags
	} //if

	// Check the scenes
	if expected, actual := 4, len(w.Scenes); actual != expected {
		t.Errorf("Expected scene count of %d but was %d", expected, actual)
	} else {
		// TODO: Check the scenes
	} //if
} //func

func TestParseFlag(t *testing.T) {
	// Lines for parsing
	f, fs, fc, comment := "flag f", "flag f set", "flag f clear", " #comment"

	// Create a parser
	p := NewParser(nil)

	// Parse a bad line
	_, err := p.parseFlag("flag")

	// Check for an error
	if err == nil {
		t.Error("Expected an error for flag line")
	} //if

	// Parse a bad line
	_, err = p.parseFlag("flag ")

	// Check for an error
	if err == nil {
		t.Error("Expected an error for flag line")
	} //if

	// Parse the flag
	flg, err := p.parseFlag(f)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the flag
	flg, err = p.parseFlag(f + comment)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse a flag with a bad value
	_, err = p.parseFlag("flag f error")

	// Check for an error
	if err == nil {
		t.Error("Expected error from bad value")
	} //if

	// Parse the flag
	flg, err = p.parseFlag(fs)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the flag
	flg, err = p.parseFlag(fs + comment)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the flag
	flg, err = p.parseFlag(fc)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check if the flag is set
	if flg.val {
		t.Error("Expected flag to not be set")
	} //if

	// Parse the flag
	flg, err = p.parseFlag(fc + comment)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "f", flg.name; actual != expected {
		t.Errorf(`Expected flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check if the flag is set
	if flg.val {
		t.Error("Expected flag to not be set")
	} //if
} //func

func TestParseScene(t *testing.T) {
	// The entire scene
	scene := `scene test:
	this is a scene
	description.
`

	// Create a parser
	p := NewParser(strings.NewReader(scene))

	// Read a line
	line, _ := p.ReadString('\n')

	// Parse the scene
	_, err := p.parseScene(line)

	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Recreate the parser
	p = NewParser(strings.NewReader(scene + "\n"))

	// Read a line
	line, _ = p.ReadString('\n')

	// Parse the scene
	s, err := p.parseScene(line)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the name
	if expected, actual := "test", s.Name; actual != expected {
		t.Errorf(`Expected scene name is "%s" but was "%s"`, expected, actual)
	} //if

	// Check the description
	if expected, actual := "this is a scene description.", s.Disp; actual != expected {
		t.Errorf(`"Expected scene description to be "%s" but was "%s"`, expected,
			actual)
	} //if
} //func

func TestParserParseChoice(t *testing.T) {
	// Choices
	c, cf, csf, ccf, ctf := "choice: an option (scene)",
		"choice (flag): an option (scene)", "choice: an option (scene) set flag",
		"choice: an option (scene) clear flag",
		"choice: an option (scene) toggle flag"

	// Create a parser
	p := NewParser(nil)

	// Parse a line that just starts with choice
	_, err := p.parseChoice("choice of the dragon")

	if err == nil {
		t.Error("Expected error for missing colon")
	} //if

	// Parse a line that has a colon but does not have a scene
	_, err = p.parseChoice("choice: an option")

	if err == nil {
		t.Error("Expected an error for a missing next scene")
	} //if

	// Parse the choice
	choice, err := p.parseChoice(c)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the display
	if expected, actual := "an option", choice.Disp; actual != expected {
		t.Errorf(`Expected the description to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the next scene
	if expected, actual := "scene", choice.Next; actual != expected {
		t.Errorf(`Expected the next scene to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Make sure checker isn't nil
	if choice.Check == nil {
		t.Error("Expected available to not be nil")
	} //if

	// Parse the choice
	choice, err = p.parseChoice(cf)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the display
	if expected, actual := "an option", choice.Disp; actual != expected {
		t.Errorf(`Expected the description to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the next scene
	if expected, actual := "scene", choice.Next; actual != expected {
		t.Errorf(`Expected the next scene to be "%s" but was "%s"`, expected,
			actual)
	} //if

	if expected, actual := "flag", choice.Check.(*flagcheck).name; actual != expected {
		t.Errorf(`Expected the flag name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the choice with a bad "command"
	_, err = p.parseChoice("choice: an option (scene) bad flag")

	// Make sure there was an error
	if err == nil {
		t.Error("Expected an error for a bad command")
	} //if

	// Parse the choice
	choice, err = p.parseChoice(csf)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the display
	if expected, actual := "an option", choice.Disp; actual != expected {
		t.Errorf(`Expected the description to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the next scene
	if expected, actual := "scene", choice.Next; actual != expected {
		t.Errorf(`Expected the next scene to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Make sure checker isn't nil
	if choice.Check == nil {
		t.Error("Expected available to not be nil")
	} //if

	// Make sure the action isn't nil
	if choice.Action == nil {
		t.Fatal("Expected action to not be nil")
	} //if

	// Get the flag action
	fa := choice.Action.(*flagact)

	// Check the action
	if expected, actual := actionSet, fa.action; actual != expected {
		t.Errorf("Expected action to be %v but was %v", expected, actual)
	} //if

	// Check the name
	if expected, actual := "flag", fa.name; actual != expected {
		t.Errorf(`Expected name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the choice
	choice, err = p.parseChoice(ccf)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the display
	if expected, actual := "an option", choice.Disp; actual != expected {
		t.Errorf(`Expected the description to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the next scene
	if expected, actual := "scene", choice.Next; actual != expected {
		t.Errorf(`Expected the next scene to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Make sure checker isn't nil
	if choice.Check == nil {
		t.Error("Expected available to not be nil")
	} //if

	// Make sure the action isn't nil
	if choice.Action == nil {
		t.Fatal("Expected action to not be nil")
	} //if

	// Get the flag action
	fa = choice.Action.(*flagact)

	// Check the action
	if expected, actual := actionClear, fa.action; actual != expected {
		t.Errorf("Expected action to be %v but was %v", expected, actual)
	} //if

	// Check the name
	if expected, actual := "flag", fa.name; actual != expected {
		t.Errorf(`Expected name to be "%s" but was "%s"`, expected, actual)
	} //if

	// Parse the choice
	choice, err = p.parseChoice(ctf)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error", err)
	} //if

	// Check the display
	if expected, actual := "an option", choice.Disp; actual != expected {
		t.Errorf(`Expected the description to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Check the next scene
	if expected, actual := "scene", choice.Next; actual != expected {
		t.Errorf(`Expected the next scene to be "%s" but was "%s"`, expected,
			actual)
	} //if

	// Make sure checker isn't nil
	if choice.Check == nil {
		t.Error("Expected available to not be nil")
	} //if

	// Make sure the action isn't nil
	if choice.Action == nil {
		t.Fatal("Expected action to not be nil")
	} //if

	// Get the flag action
	fa = choice.Action.(*flagact)

	// Check the action
	if expected, actual := actionToggle, fa.action; actual != expected {
		t.Errorf("Expected action to be %v but was %v", expected, actual)
	} //if

	// Check the name
	if expected, actual := "flag", fa.name; actual != expected {
		t.Errorf(`Expected name to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestParserErrAt(t *testing.T) {
	// Create the message
	msg := "an error"

	// Create a parser
	var p Parser

	// Create an error
	err := p.errAt(msg)

	// Check the message
	if expected, actual := msg+" at line: 0", err.Error(); actual != expected {
		t.Errorf(`Expected the line to be "%s" but was "%s"`, expected, actual)
	} //if

	// Change the line
	p.line = 4
	err = p.errAt(msg)

	// Check the message
	if expected, actual := msg+" at line: 4", err.Error(); actual != expected {
		t.Errorf(`Expected the line to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestParserErrAtF(t *testing.T) {
	// Create the message
	format, a1, a2 := "an error %s %d", "bob", 2

	// Create a parser
	var p Parser

	// Create an error
	err := p.errAtf(format, a1, a2)

	// Check the message
	if expected, actual := "an error bob 2 at line: 0", err.Error(); actual != expected {
		t.Errorf(`Expected the line to be "%s" but was "%s"`, expected, actual)
	} //if

	// Change the line
	p.line = 4
	err = p.errAtf(format, a1, a2)

	// Check the message
	if expected, actual := "an error bob 2 at line: 4", err.Error(); actual != expected {
		t.Errorf(`Expected the line to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestStripComment(t *testing.T) {
	// Lines
	noc, wc := "test", "test #comment"

	// Check the line without a comment
	if expected, actual := noc, stripComment(noc); actual != expected {
		t.Errorf(`Expected line to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the line with a comment
	if expected, actual := noc, stripComment(wc); actual != expected {
		t.Errorf(`Expected line to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
