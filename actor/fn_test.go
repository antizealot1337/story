package actor

import (
	"testing"

	"bitbucket.org/antizealot1337/story"
)

var _ story.Actor = (*Fn)(nil)

func TestFnAct(t *testing.T) {
	var (
		v = false
		f = Fn(func() { v = true })
	)

	// Call act
	f.Act()

	// Check the value
	if !v {
		t.Error("Expected value to be changed to true")
	} //if
} //func
