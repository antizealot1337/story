package actor

// Fn implements Actor for functions.
type Fn func()

// Act using the provided function.
func (f Fn) Act() { f() }
