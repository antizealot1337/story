// Package actor contains a collection of ready to use actors for a story.
//
// Actors implement story.Actor and are used in a story.Scene for an action for
// a choice. Currently only a function actor (Fn) exists.
package actor
