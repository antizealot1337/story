package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/antizealot1337/story"
	"bitbucket.org/antizealot1337/story/parser"
	"bitbucket.org/antizealot1337/story/teller"
	"bitbucket.org/antizealot1337/story/teller/terminal"
)

var (
	world story.Story
	tell  teller.Teller
)

func main() {
	// Setup command line arguments
	verbose := flag.Bool("verbose", false, "Outputs more information")

	// Create a usage
	flag.Usage = func() {
		fmt.Printf("Usage: %s [FILENAME]\n", os.Args[0])
		flag.PrintDefaults()
	}

	// Parse the flags
	flag.Parse()

	// Check the number of arguments
	if flag.NArg() == 0 {
		if *verbose {
			fmt.Println("Reading from stdin.")
		} //if

		// Read from standard input
		var err error
		world, err = parser.NewParser(os.Stdin).Parse()

		// Check for an error
		if err != nil {
			panic(err)
		} //if
	} else {
		if *verbose {
			fmt.Println("Reading from", flag.Arg(0))
		} //if

		// Open the file at the path
		f, err := os.Open(flag.Arg(0))

		// Check for an error
		if err != nil {
			panic(err)
		} //if

		// Close the file
		defer f.Close()

		if *verbose {
			fmt.Println("Interpreting.")
		} //if

		// Parse the file
		world, err = parser.NewParser(f).Parse()

		// Check for an error
		if err != nil {
			panic(err)
		} //if
	} //if

	// Create the teller
	tell = terminal.NewTeller(os.Stdin, os.Stdout)

	if *verbose {
		fmt.Println("Start playing")
	} //if

	// Tell the story
	tell.Tell(world)

	if *verbose {
		fmt.Println("End playing")
	} //if

	// Print game over to let the user know the game is over
	fmt.Println("GAME OVER!")
} //func
