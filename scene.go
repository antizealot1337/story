package story

// Scene is a single scene of the Story with a name, description and Choices. A
// Scene name should be unique to the Story.
type Scene struct {
	Name    string
	Disp    string
	Choices []Choice
} //struct

// Filter will return only the available Choices.
func (s Scene) Filter() []Choice {
	// Create a new slice to return
	rc := make([]Choice, 0, len(s.Choices))

	// Loop through the choices
	for _, c := range s.Choices {
		// Check if the choice is available
		if c.Avail() {
			// Add the choice to the slice
			rc = append(rc, c)
		} //if
	} //for

	return rc
} //func
