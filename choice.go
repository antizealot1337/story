package story

// Choice is a choice for a Scene.
type Choice struct {
	Check  Checker
	Disp   string
	Next   string
	Action Actor
} //struct

// Avail returns true if a Choice is available. Availablity is determined by the
// Check field. If no Checker is set true is returned. If the Checker is present
// it is consulted to see if the Choice is available.
func (c Choice) Avail() bool {
	if c.Check == nil {
		return true
	} //if
	return c.Check.Check()
} //func

// Choose is called when a Choice is chosen. It will call the Choice Actioner if
// one is set and returns Next.
func (c Choice) Choose() string {
	if c.Action != nil {
		c.Action.Act()
	} //if
	return c.Next
} //func
