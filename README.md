# Story

[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/story?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/story)

Story is both a package providing Go programs with some primitives and
abstractions for creating simple branching stories and a mini-language and
interpreter for reading and playing "story files". It can be used either way.

## Installation

If interested in using as a library run
`go get -u bitbucket.org/antizealot1337/story`. To install the story teller binary
run `go get -u bitbucket.org/antizealot1337/story/cmd/storyteller`. Running to
install the binary will also add the library package to your `GOPATH` so it in
effect allows for both usages.

If `GOBIN` is in your `PATH` you are good to go. If not you will have to add it
or move `storyteller` to a location in your `PATH`.

## Using story as a library
Using the story package as a library is covered in greater detail in at in the
package documentation so what follows is a very basic overview. A `Story` is an
interface that abstracts the required functionality for telling a story (mainly
used for `teller.Teller` implementations). Most programs will use `Stage` since
it implements `Story`. a `Stage` is made of `Scene`s. A `Scene` has a name,
display, and zero or more `Choice`s. These `Choice`s have the name of the next
scene and display information. They are visible by default by may be hidden by
their `Checker`. Once a `Choice` is selected it will call its `Actor` and the
`Stage`/`Story` will progress to the next `Scene`.

For information on building the `Stage`, `Scene`, `Choice` and the `Store`,
`Actor`, `Checker` interfaces please refer to
https://godoc.org/bitbucket.org/antizealot1337/story. Some commonly used `Actor`
implementations are available it the `actor` package
https://godoc.org/bitbucket.org/antizealot1337/story/actor and some common
`Checker`s are available in the `checker` package
https://godoc.org/bitbucket.org/antizealot1337/story/checker.

## Using the mini-language and interpreter
Using the mini-language and interpreter requires installing the __storyteller__
program. The mini-language is read by the __storyteller__ program and is played
with input and output at the command line or terminal.

For example assume a file called *test.story* existed with the following
contents:
```
# An example story
flag F1
flag F2 set
flag F3 clear

# This is a simple comment that takes up a whole line

scene SCENE1: # This comment will be ignored too
  This is a scene in the story. It will have choices and since it is the
  first it will be the first one presented to the player.
  choice: This is the players first choice (SCENE2)

scene SCENE2:
  This scene is only shown to the user if the chose the first option. It will
  be the end of the "game" since it contains no choices.

scene SCENE3:
  This scene has choices that are only available if F2 is set and a choice
  that will change the F3 flag.
  choice(F2): This is only available if F2 is set (SCENE4)
  choice: This will flip the F3 flag (SCENE4) set F3

scene SCENE4:
  This is an awesome scene. It is also the end of the game.
```

A player would run play the story with the following command
`storyteller /path/to/test.story`. This assumes the `storyteller` program was
installed. `/path/to/test.story` is the path to the file *test.story*.

For more information about how to write a story file refer to the parser
documentation at https://godoc.org/bitbucket.org/antizealot1337/story/parser.

## Contributing
Contributions welcome. Just send a pull request.

## License
All files in this repository are subject to the terms of the MIT license
available in the __LICENSE__ file and at https://opensource.org/licenses/MIT.
