package story

// Story is a collection of Scenes to be told to the player/user.
type Story interface {
	Get(name string) *Scene
	First() *Scene
} //interface
