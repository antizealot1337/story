package story

import (
	"testing"

	"bitbucket.org/antizealot1337/story/checker"
)

func TestSceneFilter(t *testing.T) {
	// Create a scene with choices
	s := Scene{
		Choices: []Choice{
			Choice{},
			Choice{},
			Choice{Check: checker.Bool(false)},
		},
	}

	// Filter the choices for the scene
	f := s.Filter()

	// Check if the filter worked
	if expected, actual := len(s.Choices)-1, len(f); actual != expected {
		t.Errorf("Expected filter to return %d but returned %d", expected, actual)
	} //if
} //func
