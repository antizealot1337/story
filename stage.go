package story

// Stage contains Scenes to tell (implement) a Story.
type Stage struct {
	Scenes []Scene
} //struct

// Get will find a Scene given the provided name. If no Scene with the name is
// found nil is returned. If an empty string is provided for name nil is
// returned.
func (w Stage) Get(name string) *Scene {
	// Prevent any scene with an empty name from being found
	if name == "" {
		return nil
	} //if

	// Loop through the scenes
	for _, scene := range w.Scenes {
		// Check for the name
		if scene.Name == name {
			return &scene
		} //if
	} //for

	// Return nil since no scene was found
	return nil
} //func

// First returns the first Scene of the Stage. If the stage has no Scenes nil is
// returned.
func (w Stage) First() *Scene {
	// Check if there are no scenes
	if len(w.Scenes) == 0 {
		return nil
	} //if
	return &w.Scenes[0]
} //func
