// Package teller only used to define how to tell a story.Story.
//
// Implementing Teller is not the only way to tell a story but makes story
// telling more portable. All subpackages must have a construct that implements
// Teller.
package teller

import "bitbucket.org/antizealot1337/story"

// Teller tells a Story.
type Teller interface {
	Tell(s story.Story) error
} //interface
