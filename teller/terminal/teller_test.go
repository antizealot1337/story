package terminal

import (
	"bytes"
	"io/ioutil"
	"testing"

	"bitbucket.org/antizealot1337/story"
	"bitbucket.org/antizealot1337/story/teller"
)

var _ teller.Teller = (*Teller)(nil)

func TestNewTeller(t *testing.T) {
	// The input and output
	var in, out bytes.Buffer

	// Create the teller
	st := NewTeller(&in, &out)

	// Check the buffered input
	if st.in == nil {
		t.Error("Expected in to be set")
	} //if

	// Check the buffered output
	if st.out == nil {
		t.Error("Expected out to be set")
	} //if
} //func

func TestTellerTell(t *testing.T) {
	// Create the story
	st := story.Stage{
		Scenes: []story.Scene{
			story.Scene{
				Name: "Scene 1",
				Disp: "Scene 1.",
				Choices: []story.Choice{
					story.Choice{
						Next: "Scene 2",
						Disp: "Goto Scene 2.",
					},
				},
			},
			story.Scene{
				Name: "Scene 2",
				Disp: "Scene 2.",
			},
		},
	}

	// The output
	var out bytes.Buffer

	// Create the teller
	tl := NewTeller(bytes.NewBufferString("1\n1\n"), &out)

	// Tell the story
	err := tl.Tell(&st)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// The expected output
	output := `Scene 1.
Choices:
1: Goto Scene 2.
>Scene 2.
`

	// Check the output
	if expected, actual := output, out.String(); actual != expected {
		t.Errorf(`Expected output:
			 "%s"
			 but was:
			 "%s"`, expected, actual)
	} //if
} //func

func TestTellerPresent(t *testing.T) {
	// Create the scene
	sc := story.Scene{
		Choices: []story.Choice{
			story.Choice{Next: "Next 1."},
			story.Choice{Next: "Next 2."},
		},
	}

	// Create the teller
	tl := NewTeller(bytes.NewBufferString("1\n"), ioutil.Discard)

	// Present the scene
	next, err := tl.present(&sc)

	// Check the error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	if expeted, actual := sc.Choices[0].Next, next; actual != expeted {
		t.Errorf(`Expected next to be "%s" but was "%s"`, expeted, actual)
	} //if

	// Create the teller
	tl = NewTeller(bytes.NewBufferString("2\n"), ioutil.Discard)

	// Present the scene
	next, err = tl.present(&sc)

	// Check the error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	if expeted, actual := sc.Choices[1].Next, next; actual != expeted {
		t.Errorf(`Expected next to be "%s" but was "%s"`, expeted, actual)
	} //if
} //func

func TestTellerDisplay(t *testing.T) {
	// The buffer
	var buff bytes.Buffer

	// Create the teller
	tl := NewTeller(nil, &buff)

	// Create a scene
	sc := story.Scene{
		Disp: "A scene.",
	}

	// Display the scene
	tl.display(&sc)

	if expected, actual := sc.Disp+"\n", string(buff.Bytes()); actual != expected {
		t.Errorf(`Expected
  "%s"
  written but was
  "%s"`, expected, actual)
	} //if
} //func

func TestTellerChoose(t *testing.T) {
	// Input and output
	var (
		in  = bytes.NewBufferString("a\n0\n3\n1\n2\n")
		out bytes.Buffer
	)

	// Create the teller
	tl := NewTeller(in, &out)

	// The choices to choose from
	cs := []story.Choice{
		story.Choice{Disp: "Choice 1."},
		story.Choice{Disp: "Choice 2."},
	}

	// Make a choice
	c, _ := tl.choose(cs)

	// Check the choice
	if expected, actual := cs[0].Disp, c.Disp; actual != expected {
		t.Errorf(`Expected choice disp to be "%s" but was "%s"`, expected, actual)
	} //if

	exout := `Choices:
1: Choice 1.
2: Choice 2.
>Invalid input.
>Invalid input.
>Invalid input.
>`

	// Check the output
	if expected, actual := exout, out.String(); actual != expected {
		t.Errorf(`Expected output to be:
      "%s"
      but was:
      "%s"`, expected, actual)
	} //if

	// Make a choice
	c, _ = tl.choose(cs)

	// Check the choice
	if expected, actual := cs[1].Disp, c.Disp; actual != expected {
		t.Errorf(`Expected choice disp to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
