// Package terminal contains is a simple terminal based story teller.
//
// Despite the name "terminal" for the package any io.Reader and io.Writer may
// be used for input and output. In any case the terminal.Teller will simply
// print the scene's display content and the display content of any available
// choices. If no choices are available the Teller will exit without an error.
// All input and output are completely UTF-8 text.
package terminal
