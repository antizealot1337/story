package terminal

import (
	"bufio"
	"fmt"
	"io"
	"strconv"

	"bitbucket.org/antizealot1337/story"
)

// Teller tells a Story though a simple terminal interface.
type Teller struct {
	in  *bufio.Scanner
	out *bufio.Writer
} //struct

// NewTeller creates a new Teller using the provided Reader and Writer.
func NewTeller(in io.Reader, out io.Writer) *Teller {
	return &Teller{
		in:  bufio.NewScanner(in),
		out: bufio.NewWriter(out),
	}
} //func

// Tell the Story.
func (t *Teller) Tell(st story.Story) (err error) {
	// Set the first scene
	sc := st.First()

	// The next scene name
	var next string

	// Loop while there is scene
	for sc != nil {
		// Show the scene
		next, err = t.present(sc)

		// Check for an error
		if err != nil {
			return err
		} //if

		// Get the next scene
		sc = st.Get(next)
	} //if
	return nil
} //func

func (t *Teller) present(sc *story.Scene) (next string, err error) {
	// Display the scene
	err = t.display(sc)

	// Check for an error
	if err != nil {
		return "", err
	} //if

	// Make the schoie
	var c story.Choice
	c, err = t.choose(sc.Filter())

	// Check for an error
	if err != nil {
		return "", err
	} //if

	// Choose the selected choice and set the name foe the next scene
	next = c.Choose()
	return
} //func

func (t *Teller) display(sc *story.Scene) (err error) {
	// Write to the output
	_, err = t.out.WriteString(fmt.Sprintf("%s\n", sc.Disp))

	// Check for an error
	if err != nil {
		return
	} //if

	// Flush the output buffer
	err = t.out.Flush()
	return
} //func

func (t *Teller) choose(choices []story.Choice) (c story.Choice, err error) {
	// Check if there are no choices
	if len(choices) == 0 {
		return
	} //if

	// Write the header
	_, err = t.out.WriteString("Choices:\n")

	// Check for an error
	if err != nil {
		return
	} //if

	// Write the options
	for i, ch := range choices {
		// Write a choice
		_, err = t.out.WriteString(fmt.Sprintf("%d: %s\n", i+1, ch.Disp))

		// Check for an error
		if err != nil {
			return
		} //if
	} //for

	// Flush the output
	if err = t.out.Flush(); err != nil {
		return
	} //if

done:
	for {

		// Scan for a string
		if t.in.Scan() {
			// Write the input indicator
			_, err = t.out.WriteString(">")

			// Check for an error
			if err != nil {
				return
			} //if

			// Flush the output
			if err = t.out.Flush(); err != nil {
				return
			} //if

			// Get the line
			line := t.in.Text()

			// Convert the line to an integer
			i, err := strconv.Atoi(line)

			// Check for an error
			if err != nil {
				t.out.WriteString("Invalid input.\n")
				continue
			} //if

			// Check the value
			if i < 1 || i > len(choices) {
				t.out.WriteString("Invalid input.\n")
				continue
			} //if

			c = choices[i-1]

			break done
		} //if
	} //for

	return
} //func
