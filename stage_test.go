package story

import "testing"

var _ Story = (*Stage)(nil)

func TestStageGet(t *testing.T) {
	// The scene name
	sceneName := "Test"

	// Create the world
	var w Stage

	// Try to find a scene that doesn't exist
	if scene := w.Get(sceneName); scene != nil {
		t.Error("Expected the scene to be nil")
	} //if

	// Add a scene
	w.Scenes = append(w.Scenes, Scene{Name: sceneName})

	// Attempt to find the scene by the name
	if scene := w.Get(sceneName); scene == nil {
		t.Error("Expected to find the scene")
	} //if

	// Add a scene without a name
	w.Scenes = append(w.Scenes, Scene{Name: ""})

	// Attempt to get a scene with an empty name
	if scene := w.Get(""); scene != nil {
		t.Error("Expected to be unable to find an scene without a name")
	} //if
} //func

func TestStageFirst(t *testing.T) {
	// Create a world
	w := Stage{}

	// Get the first scene
	s := w.First()

	if s != nil {
		t.Error("Expected empty world first scene to return nil")
	} //if

	// Add a scene
	w.Scenes = append(w.Scenes, Scene{})

	// Get the first scene
	s = w.First()

	if s == nil {
		t.Error("Expected a scene to be returned")
	} //if
} //func
