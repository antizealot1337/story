package story

// Checker will return a bool that indicates if something checks out or not.
type Checker interface {
	Check() bool
} //interface
