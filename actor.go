package story

// Actor can act.
type Actor interface {
	Act()
} //interface
